#   ________
#  /_  __/ /_  ____  ____ ___  ____  _________  ____
#   / / / __ \/ __ \/ __ `__ \/ __ \/ ___/ __ \/ __ \
#  / / / / / / /_/ / / / / / / /_/ (__  ) /_/ / / / /
# /_/ /_/ /_/\____/_/ /_/ /_/ .___/____/\____/_/ /_/
#                          /_/
# Tomáš Vesecký
# Email: vesecky@proton.me
# GitHub: veseckytomas
# GitLab: vesecky.tomas
{...}: {
  imports = [
    # ./awesome.nix
    # ./cinnamon.nix
    # ./deepin.nix
    # ./enlightenment.nix
    # ./gnome.nix
    ./hyprland.nix
    # ./i3.nix
    # ./plasma.nix
    ./qtile.nix
    # ./xfce.nix
    # ./xmonad.nix
  ];
}
