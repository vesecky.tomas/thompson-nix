#   ________
#  /_  __/ /_  ____  ____ ___  ____  _________  ____
#   / / / __ \/ __ \/ __ `__ \/ __ \/ ___/ __ \/ __ \
#  / / / / / / /_/ / / / / / / /_/ (__  ) /_/ / / / /
# /_/ /_/ /_/\____/_/ /_/ /_/ .___/____/\____/_/ /_/
#                          /_/
# Tomáš Vesecký
# Email: vesecky@proton.me
# GitHub: veseckytomas
# GitLab: vesecky.tomas
{...}: {
  programs.git = {
    enable = true;
    userEmail = "vesecky@proton.me";
    userName = "Tomáš Vesecký";
    signing = {
      signByDefault = true;
      key = "86020E9B84233A43E5249EC4E397E45D1BC4E984";
    };
    aliases = {
      st = "status";
      ci = "commit";
      co = "switch";
      swn = "switch -c";
      brd = "branch -D";
      br = "branch";
      ll = "log --oneline --graph --all --decorate";
      f = "fetch -ap";
      ra = "remote add origin";
      ca = "!git add --all && git commit";
      cp = "cherry-pick";
      c = "clean -Xdf";
      r = "restore --staged :/";
      re = "restore :/";
    };
    extraConfig = {
      core = {
        editor = "nvim";
      };
      push = {
        autoSetupRemote = true;
      };
      rerere = {
        enable = true;
      };
      init = {
        defaultBranch = "master";
      };
      advice = {
        addEmptyPathspec = "false";
      };
    };
  };
  catppuccin.delta = {
    enable = true;
    flavor = "mocha";
  };
  home.file."Git/migrate.sh".source = ./migrate.sh;
}
