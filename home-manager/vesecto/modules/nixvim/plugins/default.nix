#   ________
#  /_  __/ /_  ____  ____ ___  ____  _________  ____
#   / / / __ \/ __ \/ __ `__ \/ __ \/ ___/ __ \/ __ \
#  / / / / / / /_/ / / / / / / /_/ (__  ) /_/ / / / /
# /_/ /_/ /_/\____/_/ /_/ /_/ .___/____/\____/_/ /_/
#                          /_/
# Tomáš Vesecký
# Email: vesecky@proton.me
# GitHub: veseckytomas
# GitLab: vesecky.tomas
{...}: {
  imports = [
    ./alpha.nix
    ./avante.nix
    # ./bufferline.nix
    ./bullets-vim.nix
    # ./ccc.nix
    ./cmp.nix
    ./cmp-buffer.nix
    # ./cmp-cmdline.nix
    ./cmp-nvim-lsp.nix
    ./cmp-path.nix
    ./chatgpt.nix
    ./codeium-nvim.nix
    # ./codesnap.nix
    ./colorizer.nix
    ./comment.nix
    ./conform-nvim.nix
    ./copilot-chat.nix
    ./copilot-cmp.nix
    ./copilot-lua.nix
    # ./copilot-vim.nix
    # ./crates.nix
    # ./dap.nix
    # ./diffview.nix
    ./fidget.nix
    ./friendly-snippets.nix
    # ./gitmessenger.nix
    ./gitsigns.nix
    ./indent-blankline.nix
    # ./kulala.nix
    ./lint.nix
    ./lsp.nix
    ./lspkind.nix
    ./lualine.nix
    ./luasnip.nix
    ./lz-n.nix
    ./markdown-preview.nix
    ./mini.nix
    ./multicursors.nix
    ./neo-tree.nix
    # ./neogen.nix
    # ./neogit.nix
    # ./neotest.nix
    ./nix.nix
    ./noice.nix
    ./notify.nix
    # ./nvim-autopairs.nix
    ./nvim-ufo.nix
    ./oil.nix
    # ./outline-nvim.nix
    ./render-markdown-nvim.nix
    ./rustaceanvim.nix
    ./sleuth.nix
    # ./snacks.nix
    ./spectre.nix
    ./telescope.nix
    ./tmux-navigator.nix
    ./todo-comments.nix
    # ./treesitter-refactor.nix
    ./treesitter.nix
    ./ts-autotag.nix
    ./undotree.nix
    ./vim-dadbod-completion.nix
    ./vim-dadbod-ui.nix
    ./vim-dadbod.nix
    ./vim-surround.nix
    ./wakatime.nix
    ./web-devicons.nix
    ./which-key.nix
  ];
}
